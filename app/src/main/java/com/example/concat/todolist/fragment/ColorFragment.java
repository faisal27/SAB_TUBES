package com.example.concat.todolist.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.concat.todolist.R;

/**
 * Created by Eka Ardilah FebriY on 12/04/2018.
 */

public class ColorFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";

    public static ColorFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        ColorFragment fragment = new ColorFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.color_fragment, container, false);
        //Now get handle to any view contained
        // within the main layout you are using
        final View someViewW = view.findViewById(R.id.changeWhite);
        final View someViewB = view.findViewById(R.id.changeBlue);
        final View someViewG =  view.findViewById(R.id.changeGreen);
        final View someViewPur = view.findViewById(R.id.changePurple);
        final View someViewPi = view.findViewById(R.id.changePink);
        final View someViewY = view.findViewById(R.id.changeYellow);

        someViewB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Find the root view
                View root = someViewB.getRootView();
                // Set the color
                root.setBackgroundColor(getResources().getColor(R.color.bgblue));
            }
        });
        someViewW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Find the root view
                View root = someViewW.getRootView();
                // Set the color
                root.setBackgroundColor(getResources().getColor(R.color.putih));
            }
        });
        someViewG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Find the root view
                View root = someViewG.getRootView();
                // Set the color
                root.setBackgroundColor(getResources().getColor(R.color.bggreen));
            }
        });
        someViewPi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Find the root view
                View root = someViewPi.getRootView();
                // Set the color
                root.setBackgroundColor(getResources().getColor(R.color.bgpink));
            }
        });
        someViewPur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Find the root view
                View root = someViewPur.getRootView();
                // Set the color
                root.setBackgroundColor(getResources().getColor(R.color.bgpurple));
            }
        });
        someViewY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Find the root view
                View root = someViewY.getRootView();
                // Set the color
                root.setBackgroundColor(getResources().getColor(R.color.bgyellow));
            }
        });

        return view;
    }
}

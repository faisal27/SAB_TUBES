package com.example.concat.todolist.activity;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Locale;

import com.example.concat.todolist.R;
import com.example.concat.todolist.Schedule.AlarmReceiver;
import com.example.concat.todolist.model.PostPutDelKegiatan;
import com.example.concat.todolist.rest.ApiClient;
import com.example.concat.todolist.rest.ApiInterface;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InsertKegiatanActivity extends AppCompatActivity {

    private EditText insertJudul, emailText;
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    private TimePickerDialog timePickerDialog;
    private Switch aSwitch;
    private EditText tanggalText;
    private EditText waktuText;
    private Button btnInsert;
    private ApiInterface mApiInterface;
    Calendar newTimeAndDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_kegiatan);
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        insertJudul = (EditText) findViewById(R.id.TambahList);
        aSwitch = (Switch) findViewById(R.id.switch1);
        tanggalText = (EditText) findViewById(R.id.tanggalText);
        waktuText = (EditText) findViewById(R.id.waktuText);
        waktuText.setKeyListener(null);
        tanggalText.setKeyListener(null);
        emailText = (EditText) findViewById(R.id.emailText);
        btnInsert = (Button) findViewById(R.id.btnInsert);
        newTimeAndDate = Calendar.getInstance();
        Intent i = getIntent();
        Bundle b = i.getExtras();
        if (b != null) {
            String nama = (String) b.get("nama");
            emailText.setText(nama);
        }
        btnInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String judul = insertJudul.getText().toString().trim();
                String email = emailText.getText().toString().trim();
                if (tanggalText.getText().toString().equals("") && waktuText.getText().toString().equals("")) {
                    if (judul.length() < 5) {
                        insertJudul.setError("Judul tidak boleh dan minimal 5 huruf");
                    } else {
                        insertNotDateAndTime(judul, email);
                    }
                } else {
                    if (judul.length() < 5) {
                        insertJudul.setError("Judul tidak boleh dan minimal 5 huruf");
                    } else {
                        insertWithDateAndTime(judul, email);
                    }
                }
            }
        });
        tanggalText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDateDialog();
            }
        });
        waktuText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTimeDialog();
            }
        });
        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    setNotification();
                } else {
                    Toast.makeText(InsertKegiatanActivity.this, "Tidak jadi", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showDateDialog() {
        Calendar calendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                newTimeAndDate.set(Calendar.YEAR, year);
                newTimeAndDate.set(Calendar.MONTH, monthOfYear);
                newTimeAndDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                tanggalText.setText(dateFormatter.format(newTimeAndDate.getTime()));
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    private void showTimeDialog() {
        Calendar calendar = Calendar.getInstance();
        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                newTimeAndDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
                newTimeAndDate.set(Calendar.MINUTE, minute);
                newTimeAndDate.set(Calendar.SECOND, 0);
                if (minute < 10) {
                    waktuText.setText(hourOfDay + ":" + "0" + minute);
                } else {
                    waktuText.setText(hourOfDay + ":" + minute);
                }
            }
        }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
        timePickerDialog.show();
    }

    private void insertNotDateAndTime(final String judul, final String email) {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String formattedDate = df.format(c);
        String strDate = sdf.format(c);
        Call<PostPutDelKegiatan> postKegiatan = mApiInterface.postKegiatan(judul, formattedDate, strDate, email);
        postKegiatan.enqueue(new Callback<PostPutDelKegiatan>() {
            @Override
            public void onResponse(Call<PostPutDelKegiatan> call, Response<PostPutDelKegiatan> response) {
                if (response.body().getStatus().equalsIgnoreCase("fail")) {
                    Toast.makeText(getApplicationContext(), "Gagal", Toast.LENGTH_LONG).show();
                } else if (response.body().getStatus().equalsIgnoreCase("success")) {
                    Toast.makeText(getApplicationContext(), "Berhasil", Toast.LENGTH_LONG).show();
                    Intent i = new Intent(InsertKegiatanActivity.this, MainActivity.class);
                    i.putExtra("nama", email);
                    startActivity(i);
                    finish();
                    insertJudul.setText("");
                }
            }

            @Override
            public void onFailure(Call<PostPutDelKegiatan> call, Throwable t) {
                Toast.makeText(InsertKegiatanActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void insertWithDateAndTime(final String judul, final String email) {
        String tanggal = tanggalText.getText().toString().trim();
        String waktu = waktuText.getText().toString().trim();
        Call<PostPutDelKegiatan> postKegiatan = mApiInterface.postKegiatan(judul, tanggal, waktu, email);
        postKegiatan.enqueue(new Callback<PostPutDelKegiatan>() {
            @Override
            public void onResponse(Call<PostPutDelKegiatan> call, Response<PostPutDelKegiatan> response) {
                if (response.body().getStatus().equalsIgnoreCase("fail")) {
                    Toast.makeText(getApplicationContext(), "Gagal", Toast.LENGTH_LONG).show();
                } else if (response.body().getStatus().equalsIgnoreCase("success")) {
                    Toast.makeText(getApplicationContext(), "Berhasil", Toast.LENGTH_LONG).show();
                    Intent i = new Intent(InsertKegiatanActivity.this, MainActivity.class);
                    i.putExtra("nama", email);
                    startActivity(i);
                    finish();
                    insertJudul.setText("");
                }
            }

            @Override
            public void onFailure(Call<PostPutDelKegiatan> call, Throwable t) {

            }
        });
    }

    private void setNotification() {
        long dateTime = newTimeAndDate.getTimeInMillis();
        if (dateTime < System.currentTimeMillis()) {
            newTimeAndDate.add(Calendar.YEAR, newTimeAndDate.get(Calendar.YEAR));
            newTimeAndDate.add(Calendar.MONTH, newTimeAndDate.get(Calendar.MONTH));
            newTimeAndDate.add(Calendar.DAY_OF_MONTH, newTimeAndDate.get(Calendar.DAY_OF_MONTH));
            newTimeAndDate.add(Calendar.HOUR_OF_DAY, newTimeAndDate.get(Calendar.HOUR_OF_DAY));
            newTimeAndDate.add(Calendar.MINUTE, newTimeAndDate.get(Calendar.MINUTE));
        }
        Intent intent = new Intent(InsertKegiatanActivity.this, AlarmReceiver.class);
        Bundle b = new Bundle();
        b.putString("nama", emailText.getText().toString());
        intent.putExtras(b);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(InsertKegiatanActivity.this, 100, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, newTimeAndDate.getTimeInMillis(), pendingIntent);
    }
}

package com.example.concat.todolist.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.concat.todolist.R;
import com.example.concat.todolist.model.PostPutDelUser;
import com.example.concat.todolist.rest.ApiClient;
import com.example.concat.todolist.rest.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity {

    private EditText emailText, passwordText, konPassText;
    private Button btnForgot;
    private ApiInterface mApiInterface;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        emailText = (EditText) findViewById(R.id.forgot_email);
        passwordText = (EditText) findViewById(R.id.forgot_password);
        konPassText = (EditText) findViewById(R.id.forgot_kon_pass);
        btnForgot = (Button) findViewById(R.id.btn_forgot);
        btnForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = emailText.getText().toString().trim();
                String pass = passwordText.getText().toString().trim();
                String konPass = konPassText.getText().toString().trim();
                if (email.isEmpty()){
                    emailText.setError("email wajib diisi");
                } else if (pass.length() < 8){
                    passwordText.setError("password wajib diisi dan minimal 8 karakter");
                } else if (konPass.length() < 8){
                    konPassText.setError("Konfirmasi password wajib diisi dan minimal 8 karakter");
                } else {
                    if (email.matches("([A-Za-z][A-Za-z0-9\\-\\.\\_]*)\\@([A-Za-z][A-Za-z0-9\\-\\_]*)(\\.[A-Za-z][A-Za-z0-9\\-\\_]*)+$")){
                        if (pass.equals(konPass)){
                            Call<PostPutDelUser> setUlangPassword = mApiInterface.putPasswordUser(pass, email);
                            setUlangPassword.enqueue(new Callback<PostPutDelUser>() {
                                @Override
                                public void onResponse(Call<PostPutDelUser> call, Response<PostPutDelUser> response) {
                                    if (response.body().getStatus().equalsIgnoreCase("success")){
                                        Toast.makeText(getApplicationContext(), "Password berhasil diubah", Toast.LENGTH_LONG).show();
                                        Intent i = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
                                        startActivity(i);
                                        finish();
                                    } else if (response.body().getStatus().equalsIgnoreCase("failed")){
                                        Toast.makeText(getApplicationContext(), "Password gagal diubah", Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<PostPutDelUser> call, Throwable t) {
                                    Toast.makeText(getApplicationContext(), "Password gagal diubah", Toast.LENGTH_LONG).show();
                                }
                            });
                        } else {
                            Toast.makeText(getApplicationContext(), "Password tidak sama", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
        });
    }
}

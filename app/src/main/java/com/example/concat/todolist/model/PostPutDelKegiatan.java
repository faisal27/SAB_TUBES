package com.example.concat.todolist.model;

import com.google.gson.annotations.SerializedName;

import com.google.gson.annotations.SerializedName;

public class PostPutDelKegiatan {

    @SerializedName("status")
    String status;
    @SerializedName("result")
    Kegiatan mKegiatan;
    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Kegiatan getmKegiatan() {
        return mKegiatan;
    }

    public void setmKegiatan(Kegiatan mKegiatan) {
        this.mKegiatan = mKegiatan;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

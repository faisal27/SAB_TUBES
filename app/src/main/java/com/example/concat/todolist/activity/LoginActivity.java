package com.example.concat.todolist.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.concat.todolist.R;
import com.example.concat.todolist.session.Session;
import com.example.concat.todolist.model.User;
import com.example.concat.todolist.rest.ApiClient;
import com.example.concat.todolist.rest.ApiInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity {

    private EditText emailText, passText;
    private Button btnMasuk, btnDaftar, btnForgot;
    private ProgressDialog progressDialog;
    private User user;
    ApiInterface mApiInterface;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        emailText = (EditText) findViewById(R.id.email_masuk);
        passText = (EditText) findViewById(R.id.kata_sandi_masuk);
        btnMasuk = (Button) findViewById(R.id.btn_masuk);
        btnForgot = (Button) findViewById(R.id.forgot_pass);
        btnDaftar = (Button) findViewById(R.id.btn_daftar);

        btnForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
            }
        });

        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegistrasiActivity.class));
            }
        });

       btnMasuk.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               progressDialog.show();
               final String email = emailText.getText().toString().trim();
               final String pass = passText.getText().toString().trim();
               Call<ResponseBody> login = mApiInterface.loginRequest(email, pass);
               login.enqueue(new Callback<ResponseBody>() {
                   @Override
                   public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                       progressDialog.dismiss();
                       if (response.isSuccessful()){
                           try {
                               JSONObject jsonRESULTS = new JSONObject(response.body().string());
                               if (email.isEmpty() && pass.isEmpty()){
                                   Toast.makeText(LoginActivity.this, "Email dan Password tidak boleh kosong", Toast.LENGTH_SHORT).show();
                               } else {
                                   if (jsonRESULTS.getString("status").equals("failed")){
                                       Toast.makeText(LoginActivity.this, "email atau password salah", Toast.LENGTH_SHORT).show();
                                   } else if (jsonRESULTS.getString("status").equals("success")){
                                       Session.createSignInSession(LoginActivity.this, email);
                                       Toast.makeText(LoginActivity.this, "Login Success", Toast.LENGTH_SHORT).show();
                                       Intent i = new Intent(LoginActivity.this, MainActivity.class);
                                       i.putExtra("nama", email);
                                       startActivity(i);
                                       finish();
                                       emailText.setText("");
                                       passText.setText("");
                                   }
                               }
                           } catch (JSONException e) {
                               e.printStackTrace();
                           } catch (IOException e) {
                               e.printStackTrace();
                           }
                       } else {
                           Toast.makeText(LoginActivity.this, "GAGAL LOGIN", Toast.LENGTH_SHORT).show();
                       }
                   }

                   @Override
                   public void onFailure(Call<ResponseBody> call, Throwable t) {

                   }
               });
           }
       });
    }
}

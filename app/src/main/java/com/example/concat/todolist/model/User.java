package com.example.concat.todolist.model;

import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("id_user")
    private String idUser;
    @SerializedName("nama_lengkap")
    private String namaLengkap;
    @SerializedName("email")
    private String email;
    @SerializedName("username")
    private String username;
    @SerializedName("password")
    private String password;
    @SerializedName("status")
    String status;

    public User() {}

    public User(String idUser, String namaLengkap, String email, String username, String password) {
        this.idUser = idUser;
        this.namaLengkap = namaLengkap;
        this.email = email;
        this.username = username;
        this.password = password;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getNamaLengkap() {
        return namaLengkap;
    }

    public void setNamaLengkap(String namaLengkap) {
        this.namaLengkap = namaLengkap;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

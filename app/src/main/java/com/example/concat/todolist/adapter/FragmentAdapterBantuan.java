package com.example.concat.todolist.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.concat.todolist.fragment.FragmentBantuanPengguna;
import com.example.concat.todolist.fragment.FragmentKontakKami;

/**
 * Created by Evi on 12-Apr-18.
 */

public class FragmentAdapterBantuan extends FragmentPagerAdapter {

    final int PAGE_BANTUAN = 2;
    private String tabTitle [] = new String[] {"Panduan Pengguna", "Kontak Kami"};

    public FragmentAdapterBantuan(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return PAGE_BANTUAN;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0){
            return FragmentBantuanPengguna.newInstance(position);
        } else {
            return FragmentKontakKami.newInstance(position);
        }
    }


    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitle[position];
    }

}

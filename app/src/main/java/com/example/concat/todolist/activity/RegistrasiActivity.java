package com.example.concat.todolist.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.concat.todolist.R;
import com.example.concat.todolist.model.PostPutDelUser;
import com.example.concat.todolist.rest.ApiClient;
import com.example.concat.todolist.rest.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrasiActivity extends AppCompatActivity {

    private EditText namaText, emailText, usernameText, passwordText, konPassText;
    private Button btnDaftar;
    private ApiInterface mApiInterface;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrasi);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Send data....");
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);

        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        namaText = (EditText) findViewById(R.id.daftar_nama_pengguna);
        emailText = (EditText) findViewById(R.id.daftar_email);
        usernameText = (EditText) findViewById(R.id.daftar_username);
        passwordText = (EditText) findViewById(R.id.daftar_password);
        konPassText = (EditText) findViewById(R.id.daftar_konpassword);
        btnDaftar = (Button) findViewById(R.id.btn_registrasi);

        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nama = namaText.getText().toString().trim();
                String email = emailText.getText().toString().trim();
                String username = usernameText.getText().toString().trim();
                String password = passwordText.getText().toString().trim();
                String konPass = konPassText.getText().toString().trim();

                if (nama.length() < 3) {
                    namaText.setError("nama minimal 3 huruf dan tidak boleh kosong");
                } else if (email.isEmpty()) {
                    emailText.setError("email tidak boleh kosong");
                } else if (username.length() < 5) {
                    usernameText.setError("username tidak boleh kosong dan minimal 5 karakter");
                } else if (password.length() < 8) {
                    passwordText.setError("password tidak boleh kosong dan minimal 8 karakter");
                } else {
                    if (nama.matches("[a-zA-Z ]+$")) {
                        if (email.matches("([A-Za-z][A-Za-z0-9\\-\\.\\_]*)\\@([A-Za-z][A-Za-z0-9\\-\\_]*)(\\.[A-Za-z][A-Za-z0-9\\-\\_]*)+$")) {
                            if (password.equals(konPass)) {
                                Call<PostPutDelUser> postUser = mApiInterface.postUser(nama, email, username, password);
                                postUser.enqueue(new Callback<PostPutDelUser>() {
                                    @Override
                                    public void onResponse(Call<PostPutDelUser> call, Response<PostPutDelUser> response) {
                                        if (response.body().getMessage().equalsIgnoreCase("username dan email sudah digunakan")) {
                                            Toast.makeText(getApplicationContext(), "Username atau email sudah digunakan", Toast.LENGTH_LONG).show();
                                        } else if (response.body().getMessage().equalsIgnoreCase("username tersedia")) {
                                            Toast.makeText(getApplicationContext(), "Data berhasil disimpan", Toast.LENGTH_LONG).show();
                                            Intent i = new Intent(RegistrasiActivity.this, LoginActivity.class);
                                            startActivity(i);
                                            finish();
                                            namaText.setText("");
                                            emailText.setText("");
                                            usernameText.setText("");
                                            passwordText.setText("");
                                            konPassText.setText("");
                                        } else if (response.body().getStatus().equalsIgnoreCase("fail")) {
                                            Toast.makeText(getApplicationContext(), "Data gagal disimpan", Toast.LENGTH_LONG).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<PostPutDelUser> call, Throwable t) {
                                        Toast.makeText(getApplicationContext(), "Data gagal disimpan", Toast.LENGTH_LONG).show();
                                    }
                                });
                            } else {
                                Toast.makeText(getApplicationContext(), "Password tidak sama", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            emailText.setError("masukan email yang benar");
                        }
                    } else {
                        namaText.setError("nama harus menggunakan huruf");
                    }
                }
            }
        });
    }
}

package com.example.concat.todolist.model;

import com.google.gson.annotations.SerializedName;

public class Kegiatan {

    @SerializedName("id_kegiatan")
    private String idKegiatan;
    @SerializedName("judul")
    private String judul;
    @SerializedName("tanggal")
    private String tanggalKegiatan;
    @SerializedName("waktu")
    private String waktuKegiatan;
    @SerializedName("email")
    private String email;
    @SerializedName("status")
    private String status;

    public Kegiatan() {
    }

    public Kegiatan(String idKegiatan, String judul, String tanggalKegiatan, String waktuKegiatan, String email) {
        this.idKegiatan = idKegiatan;
        this.judul = judul;
        this.tanggalKegiatan = tanggalKegiatan;
        this.waktuKegiatan = waktuKegiatan;
        this.email = email;
    }

    public String getIdKegiatan() {
        return idKegiatan;
    }

    public void setIdKegiatan(String idKegiatan) {
        this.idKegiatan = idKegiatan;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getTanggalKegiatan() {
        return tanggalKegiatan;
    }

    public void setTanggalKegiatan(String tanggalKegiatan) {
        this.tanggalKegiatan = tanggalKegiatan;
    }

    public String getWaktuKegiatan() {
        return waktuKegiatan;
    }

    public void setWaktuKegiatan(String waktuKegiatan) {
        this.waktuKegiatan = waktuKegiatan;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

package com.example.concat.todolist.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.concat.todolist.R;
import com.example.concat.todolist.activity.MainActivity;
import com.example.concat.todolist.model.PostPutDelUser;
import com.example.concat.todolist.rest.ApiClient;
import com.example.concat.todolist.rest.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Eka Ardilah FebriY on 12/04/2018.
 */

public class ChangePasswordFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    private EditText passText;
    private EditText konPassText;
    private Button btnUbahSandi;
    private ApiInterface mApiInterface;

    public static ChangePasswordFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        ChangePasswordFragment fragment = new ChangePasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.change_password_fragment, container, false);
        passText = (EditText) view.findViewById(R.id.sandiBaru);
        konPassText = (EditText) view.findViewById(R.id.konfSandiBaru);
        btnUbahSandi = (Button) view.findViewById(R.id.btnUbahSandi);
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        Intent i = getActivity().getIntent();
        Bundle b = i.getExtras();
        if (b != null){
            final String email = (String) b.get("nama");
            btnUbahSandi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String sandiBaru = passText.getText().toString().trim();
                    String konSandiBaru = konPassText.getText().toString().trim();
                    if (sandiBaru.length() < 8){
                        passText.setError("Password harus 8 karakter dan tidak boleh kosong");
                    } else if (konSandiBaru.length() < 8){
                        konPassText.setError("Konfirmasi Password harus 8 karakter dan tidak boleh kosong");
                    } else {
                        if (sandiBaru.equals(konSandiBaru)){
                            Call<PostPutDelUser> ubahSandi = mApiInterface.putPasswordUser(sandiBaru, email);
                            ubahSandi.enqueue(new Callback<PostPutDelUser>() {
                                @Override
                                public void onResponse(Call<PostPutDelUser> call, Response<PostPutDelUser> response) {
                                    if (response.body().getStatus().equalsIgnoreCase("failed")){
                                        Toast.makeText(getActivity(), "Password gagal diubah", Toast.LENGTH_LONG).show();
                                    } else if (response.body().getStatus().equalsIgnoreCase("success")){
                                        Toast.makeText(getActivity(), "Password berhasil diubah", Toast.LENGTH_LONG).show();
                                        Intent i = new Intent(getActivity(), MainActivity.class);
                                        i.putExtra("nama", email);
                                        startActivity(i);
                                        getActivity().finish();
                                        passText.setText("");
                                        konPassText.setText("");
                                    }
                                }

                                @Override
                                public void onFailure(Call<PostPutDelUser> call, Throwable t) {

                                }
                            });
                        }
                    }
                }
            });
        }
        return view;
    }
}

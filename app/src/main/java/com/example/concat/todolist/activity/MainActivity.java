package com.example.concat.todolist.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.concat.todolist.R;
import com.example.concat.todolist.adapter.KegiatanAdapter;
import com.example.concat.todolist.model.Kegiatan;
import com.example.concat.todolist.model.TampilKegiatan;
import com.example.concat.todolist.rest.ApiClient;
import com.example.concat.todolist.rest.ApiInterface;
import com.example.concat.todolist.session.Session;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private TextView namaText;
    private FloatingActionButton btnInsert;
    private FloatingActionButton btnSetting;
    ApiInterface mApiInterface;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public static MainActivity ma;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        namaText = (TextView) findViewById(R.id.nama);
        btnInsert = (FloatingActionButton) findViewById(R.id.button_plus);
        btnSetting = (FloatingActionButton) findViewById(R.id.button_profil);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        ma=this;
        Intent i = getIntent();
        Bundle b = i.getExtras();
        if (b != null){
            String nama = (String) b.get("nama");
            namaText.setText(nama);
        }
        String email = namaText.getText().toString();
        Call<TampilKegiatan> kegiatan = mApiInterface.getKegiatan(email);
        kegiatan.enqueue(new Callback<TampilKegiatan>() {
            @Override
            public void onResponse(Call<TampilKegiatan> call, Response<TampilKegiatan>
                    response) {
                List<Kegiatan> userList = response.body().getListDataKegiatan();
                Log.d("Retrofit Get", "Jumlah data Kegiatan: " +
                        String.valueOf(userList.size()));
                mAdapter = new KegiatanAdapter(userList);
                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onFailure(Call<TampilKegiatan> call, Throwable t) {
                Log.e("Retrofit Get", t.toString());
            }
        });
        btnInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, InsertKegiatanActivity.class);
                String nama = namaText.getText().toString();
                i.putExtra("nama", nama);
                startActivity(i);
                finish();
            }
        });
        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(MainActivity.this, btnSetting);
                popup.getMenuInflater().inflate(R.menu.menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()){
                            case R.id.pengaturan :
                                Intent i = new Intent(MainActivity.this, SettingActivity.class);
                                  String nama = namaText.getText().toString();
                                i.putExtra("nama", nama);
                                startActivity(i);
                                break;
                            case R.id.bantuan :
                                Intent intent = new Intent(MainActivity.this, BantuanActivity.class);
                                startActivity(intent);
                                break;
                            case R.id.keluar :
                                new AlertDialog.Builder(MainActivity.this)
                                        .setTitle("LOGOUT")
                                        .setMessage("Apakah Anda Yakin Logout?")
                                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                Session.logout(MainActivity.this);
                                                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                                                finish();
                                            }

                                        })
                                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                Toast.makeText(MainActivity.this, "Logout Gagal", Toast.LENGTH_LONG).show();
                                            }

                                        })
                                        .show();
                                break;
                        }
                        return true;
                    }
                });
                popup.show();
            }

        });
    }
}

package com.example.concat.todolist.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.concat.todolist.fragment.ChangePasswordFragment;
import com.example.concat.todolist.fragment.ColorFragment;
import com.example.concat.todolist.fragment.ProfileFragment;

/**
 * Created by Eka Ardilah FebriY on 03/03/2018.
 */

public class SampleFragmentPagerAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 3;
    private String tabTitles[] = new String[] { "Profil", "Kata Sandi", "Latar" };

    public SampleFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        if(position==0){
            return ProfileFragment.newInstance(position);
        }else if(position==1){
            return ChangePasswordFragment.newInstance(position);
        }else{
            return ColorFragment.newInstance(position);
        }

    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }

}

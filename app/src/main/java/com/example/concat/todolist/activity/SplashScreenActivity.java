package com.example.concat.todolist.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.concat.todolist.R;
import com.example.concat.todolist.session.Session;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        ImageView imageView = findViewById(R.id.ImageConcat);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade);
        imageView.startAnimation(animation);

        Thread time = new Thread() {
            public void run() {
                try {
                    sleep(3000);
                    super.run();
                    Intent intent;
                    SharedPreferences preferences = SplashScreenActivity.this.getSharedPreferences(Session.PREF_NAME, 0);
                    try {
                        String id = preferences.getString("email", null);
                        Log.e("email", id);
                        intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.putExtra("nama", id);
                    } catch (Exception e) {
                        intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                }
                    startActivity(intent);
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

            }
        };

        time.start();
    }
}

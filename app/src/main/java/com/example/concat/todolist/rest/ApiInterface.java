package com.example.concat.todolist.rest;

import android.text.format.Time;

import com.example.concat.todolist.model.PostPutDelKegiatan;
import com.example.concat.todolist.model.PostPutDelUser;
import com.example.concat.todolist.model.TampilKegiatan;
import com.example.concat.todolist.model.TampilUser;
import com.example.concat.todolist.model.User;

import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("user_android?")
    Call<ResponseBody> getUser(@Query("email") String email);

    @GET("kegiatan?")
    Call<TampilKegiatan> getKegiatan(@Query("email") String email);

    @FormUrlEncoded
    @POST("user")
    Call<PostPutDelUser> postUser(@Field("nama_lengkap") String nama,
                                  @Field("email") String email,
                                  @Field("username") String username,
                                  @Field("password") String password);

    @FormUrlEncoded
    @POST("kegiatan")
    Call<PostPutDelKegiatan> postKegiatan(@Field("judul") String judul,
                                          @Field("tanggal") String tanggal,
                                          @Field("waktu") String waktu,
                                          @Field("email") String email);


    @FormUrlEncoded
    @PUT("kegiatan")
    Call<PostPutDelKegiatan> putKegiatan(@Field("id_kegiatan") String id,
                                         @Field("judul") String judul,
                                         @Field("tanggal") String tanggal,
                                         @Field("waktu") String waktu);

    @FormUrlEncoded
    @PUT("user")
    Call<PostPutDelUser> putUser(@Field("id_user") String id,
                                 @Field("nama_lengkap") String nama,
                                 @Field("email") String email,
                                 @Field("username") String username);

    @FormUrlEncoded
    @PUT("password")
    Call<PostPutDelUser> putPasswordUser(@Field("password") String password,
                                         @Field("email") String email);

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "user", hasBody = true)
    Call<PostPutDelUser> deleteUser(@Field("id_user") String id);

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "kegiatan", hasBody = true)
    Call<PostPutDelKegiatan> deleteKegiatan(@Field("id_kegiatan") String id);

    @FormUrlEncoded
    @POST("login")
    Call<ResponseBody> loginRequest(@Field("email") String email,
                                    @Field("password") String password);
}

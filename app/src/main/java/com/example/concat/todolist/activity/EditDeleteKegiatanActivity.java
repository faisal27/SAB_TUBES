package com.example.concat.todolist.activity;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.concat.todolist.R;
import com.example.concat.todolist.Schedule.AlarmReceiver;
import com.example.concat.todolist.model.PostPutDelKegiatan;
import com.example.concat.todolist.rest.ApiClient;
import com.example.concat.todolist.rest.ApiInterface;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditDeleteKegiatanActivity extends AppCompatActivity {
    private EditText idText;
    private EditText judulText;
    private EditText tanggalText;
    private EditText waktuText;
    private EditText emailText;
    private Button btnUpdate;
    private Button btnDelete;
    private Switch aSwitch;
    private ApiInterface mApiInterface;
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    private TimePickerDialog timePickerDialog;
    Calendar newTimeAndDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_delete_kegiatan);
        idText = (EditText) findViewById(R.id.idList);
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        judulText = (EditText) findViewById(R.id.editList);
        aSwitch = (Switch) findViewById(R.id.switch2);
        tanggalText = (EditText) findViewById(R.id.tanggalEditText);
        waktuText = (EditText) findViewById(R.id.waktuEditText);
        emailText = (EditText) findViewById(R.id.emailTextEdit);
        btnUpdate = (Button) findViewById(R.id.btnUpdateKegiatan);
        btnDelete = (Button) findViewById(R.id.btnDeleteKegiatan);
        newTimeAndDate = Calendar.getInstance();
        waktuText.setKeyListener(null);
        tanggalText.setKeyListener(null);
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        Intent i = getIntent();
        idText.setText(i.getStringExtra("IdKegiatan"));
        idText.setTag(idText.getKeyListener());
        idText.setKeyListener(null);
        judulText.setText(i.getStringExtra("Judul"));
        tanggalText.setText(i.getStringExtra("Tanggal"));
        waktuText.setText(i.getStringExtra("Waktu"));
        emailText.setText(i.getStringExtra("Email"));
        final String email = emailText.getText().toString().trim();
        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    setNotification();
                }
            }
        });
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String id = idText.getText().toString().trim();
                final String judul = judulText.getText().toString().trim();
                final String tanggal = tanggalText.getText().toString().trim();
                final String waktu = waktuText.getText().toString().trim();
                if (judul.length() < 5){
                    judulText.setError("Judul tidak boleh kosong minimal 5 huruf");
                } else if (tanggal.isEmpty()) {
                    tanggalText.setError("tanggal tidak boleh kosong");
                } else if (waktu.isEmpty()){
                    waktuText.setError("waktu tidak boleh kosong");
                } else {
                    if (judul.matches("[a-zA-Z ]+$")){
                        Call<PostPutDelKegiatan> putKegiatan = mApiInterface.putKegiatan(id, judul, tanggal, waktu);
                        putKegiatan.enqueue(new Callback<PostPutDelKegiatan>() {
                            @Override
                            public void onResponse(Call<PostPutDelKegiatan> call, Response<PostPutDelKegiatan> response) {
                                Toast.makeText(getApplicationContext(), "Berhasil", Toast.LENGTH_LONG).show();
                                Intent i = new Intent(EditDeleteKegiatanActivity.this, MainActivity.class);
                                i.putExtra("nama", email);
                                startActivity(i);
                                finish();
                            }

                            @Override
                            public void onFailure(Call<PostPutDelKegiatan> call, Throwable t) {
                                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                            }
                        });
                    } else {
                        judulText.setError("Judul hanya boleh huruf");
                    }
                }
            }
        });
        tanggalText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDateDialog();
            }
        });
        waktuText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTimeDialog();
            }
        });
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (idText.getText().toString().trim().isEmpty() == false) {
                    new AlertDialog.Builder(EditDeleteKegiatanActivity.this)
                            .setTitle("DELETE")
                            .setMessage("Apakah Anda Yakin Menghapus ?")
                            .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Call<PostPutDelKegiatan> deleteUser = mApiInterface.deleteKegiatan(idText.getText().toString());
                                    deleteUser.enqueue(new Callback<PostPutDelKegiatan>() {
                                        @Override
                                        public void onResponse(Call<PostPutDelKegiatan> call, Response<PostPutDelKegiatan> response) {
                                            Toast.makeText(EditDeleteKegiatanActivity.this, "Hapus Sukses", Toast.LENGTH_LONG).show();
                                            Intent i = new Intent(EditDeleteKegiatanActivity.this, MainActivity.class);
                                            i.putExtra("nama", email);
                                            startActivity(i);
                                            finish();
                                        }

                                        @Override
                                        public void onFailure(Call<PostPutDelKegiatan> call, Throwable t) {
                                            Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            })
                            .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(EditDeleteKegiatanActivity.this, "Hapus Gagal", Toast.LENGTH_LONG).show();
                                }

                            })
                            .show();

                } else {
                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void showDateDialog() {
        Calendar calendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                newTimeAndDate.set(Calendar.YEAR, year);
                newTimeAndDate.set(Calendar.MONTH, monthOfYear);
                newTimeAndDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                tanggalText.setText(dateFormatter.format(newTimeAndDate.getTime()));
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    private void showTimeDialog() {
        Calendar calendar = Calendar.getInstance();
        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                newTimeAndDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
                newTimeAndDate.set(Calendar.MINUTE, minute);
                newTimeAndDate.set(Calendar.SECOND, 0);
                if (minute < 10) {
                    waktuText.setText(hourOfDay + ":" + "0" + minute);
                } else {
                    waktuText.setText(hourOfDay + ":" + minute);
                }
            }
        }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
        timePickerDialog.show();
    }

    private void setNotification() {
        long dateTime = newTimeAndDate.getTimeInMillis();
        if (dateTime < System.currentTimeMillis()) {
            newTimeAndDate.add(Calendar.YEAR, newTimeAndDate.get(Calendar.YEAR));
            newTimeAndDate.add(Calendar.MONTH, newTimeAndDate.get(Calendar.MONTH));
            newTimeAndDate.add(Calendar.DAY_OF_MONTH, newTimeAndDate.get(Calendar.DAY_OF_MONTH));
            newTimeAndDate.add(Calendar.HOUR_OF_DAY, newTimeAndDate.get(Calendar.HOUR_OF_DAY));
            newTimeAndDate.add(Calendar.MINUTE, newTimeAndDate.get(Calendar.MINUTE));
        }
        Intent intent = new Intent(EditDeleteKegiatanActivity.this, AlarmReceiver.class);
        Bundle b = new Bundle();
        b.putString("nama", emailText.getText().toString());
        intent.putExtras(b);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(EditDeleteKegiatanActivity.this, 100, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, newTimeAndDate.getTimeInMillis(), pendingIntent);
    }
}

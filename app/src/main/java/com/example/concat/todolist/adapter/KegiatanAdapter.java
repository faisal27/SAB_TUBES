package com.example.concat.todolist.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.concat.todolist.R;
import com.example.concat.todolist.activity.EditDeleteKegiatanActivity;
import com.example.concat.todolist.model.Kegiatan;

import java.util.List;

public class KegiatanAdapter extends RecyclerView.Adapter<KegiatanAdapter.MyViewHolder> {
    List<Kegiatan> mKegiatanList;

    public KegiatanAdapter(List<Kegiatan> KegiatanList) {
        mKegiatanList = KegiatanList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.kegiatan_list, parent, false);
        MyViewHolder mViewHolder = new MyViewHolder(mView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.mTextViewJudul.setText(mKegiatanList.get(position).getJudul());
        holder.mTextViewTanggal.setText(mKegiatanList.get(position).getTanggalKegiatan());
        holder.mTextViewWaktu.setText(mKegiatanList.get(position).getWaktuKegiatan());
        holder.mTextViewId.setText(mKegiatanList.get(position).getIdKegiatan());
        holder.mTextViewEmail.setText(mKegiatanList.get(position).getEmail());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(view.getContext(), EditDeleteKegiatanActivity.class);
                mIntent.putExtra("IdKegiatan", mKegiatanList.get(position).getIdKegiatan());
                mIntent.putExtra("Judul", mKegiatanList.get(position).getJudul());
                mIntent.putExtra("Tanggal", mKegiatanList.get(position).getTanggalKegiatan());
                mIntent.putExtra("Waktu", mKegiatanList.get(position).getWaktuKegiatan());
                mIntent.putExtra("Email", mKegiatanList.get(position).getEmail());
                view.getContext().startActivity(mIntent);
                ((Activity)view.getContext()).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mKegiatanList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextViewJudul, mTextViewTanggal, mTextViewId, mTextViewWaktu, mTextViewEmail;

        public MyViewHolder(View itemView) {
            super(itemView);
            mTextViewJudul = (TextView) itemView.findViewById(R.id.judulText);
            mTextViewTanggal = (TextView) itemView.findViewById(R.id.tanggalText);
            mTextViewId = (TextView) itemView.findViewById(R.id.idKegiatanText);
            mTextViewWaktu = (TextView) itemView.findViewById(R.id.waktuKegiatanText);
            mTextViewEmail = (TextView) itemView.findViewById(R.id.emaiKegiatantext);
        }
    }
}

package com.example.concat.todolist.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TampilKegiatan {
    @SerializedName("status")
    String status;
    @SerializedName("result")
    List<Kegiatan> listDataKegiatan;
    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Kegiatan> getListDataKegiatan() {
        return listDataKegiatan;
    }

    public void setListDataKegiatan(List<Kegiatan> listDataKegiatan) {
        this.listDataKegiatan = listDataKegiatan;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

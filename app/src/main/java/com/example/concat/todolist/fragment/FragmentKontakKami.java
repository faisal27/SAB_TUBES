package com.example.concat.todolist.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.concat.todolist.R;

/**
 * Created by Evi on 12-Apr-18.
 */

public class FragmentKontakKami extends Fragment {

    public static final String ARG_PAGE = "ARG_PAGE";

    public static FragmentKontakKami newInstance(int page){
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        FragmentKontakKami fragment = new FragmentKontakKami();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_kontak_kami, container, false);
        return view;
    }

}

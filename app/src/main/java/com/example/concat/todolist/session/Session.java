package com.example.concat.todolist.session;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Muhamad Faisal I A on 17/03/2018.
 */

public class Session {
    public static SharedPreferences preferences;
    public static SharedPreferences.Editor editor;
    public static String PREF_NAME = "DataUser";

    public static void createSignInSession(Context context, String email){
        preferences = context.getSharedPreferences(PREF_NAME, 0);
        editor = preferences.edit();
        editor.putString("email", email);
        editor.commit();
    }

    public static void logout(Context context){
        preferences = context.getSharedPreferences(PREF_NAME, 0);
        editor = preferences.edit();
        editor.clear();
        editor.commit();
    }
}

package com.example.concat.todolist.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.concat.todolist.R;
import com.example.concat.todolist.activity.MainActivity;
import com.example.concat.todolist.model.PostPutDelUser;
import com.example.concat.todolist.model.User;
import com.example.concat.todolist.rest.ApiClient;
import com.example.concat.todolist.rest.ApiInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Eka Ardilah FebriY on 12/04/2018.
 */

public class ProfileFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    private EditText idText;
    private EditText namaText;
    private EditText emailText;
    private EditText usernameText;
    private Button btnUpdateProfile;
    private ApiInterface mApiInterface;

    public static ProfileFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile_fragment, container, false);
        idText = (EditText) view.findViewById(R.id.idProfileText);
        namaText = (EditText) view.findViewById(R.id.namaProfileText);
        emailText = (EditText) view.findViewById(R.id.emailProfileText);
        usernameText = (EditText) view.findViewById(R.id.usernameProfileText);
        btnUpdateProfile = (Button) view.findViewById(R.id.simpanPengaturanEdit);
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        idText.setEnabled(false);
        emailText.setEnabled(false);
        Intent i = getActivity().getIntent();
        Bundle b = i.getExtras();
        if (b != null){
            final String email = (String) b.get("nama");
            Call<ResponseBody> userList = mApiInterface.getUser(email);
            userList.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()){
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().string());
                            JSONArray jsonArray = jsonObject.getJSONArray("result");
                            if (jsonObject.getString("status").equals("success")){
                                JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                                idText.setText(jsonObject1.getString("id_user"));
                                namaText.setText(jsonObject1.getString("nama_lengkap"));
                                emailText.setText(jsonObject1.getString("email"));
                                usernameText.setText(jsonObject1.getString("username"));
                            }
                        } catch (JSONException e){

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
            btnUpdateProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String sid = idText.getText().toString().trim();
                    final String snama = namaText.getText().toString().trim();
                    final String semail = emailText.getText().toString().trim();
                    final String susername = usernameText.getText().toString().trim();
                    Call<PostPutDelUser> editUser = mApiInterface.putUser(sid,snama, semail, susername);
                    editUser.enqueue(new Callback<PostPutDelUser>() {
                        @Override
                        public void onResponse(Call<PostPutDelUser> call, Response<PostPutDelUser> response) {
                           if (snama.length() < 3){
                               namaText.setError("Nama Tidak boleh kosong dan minimal 3 huruf");
                           } else if (semail.isEmpty()){
                               emailText.setError("Email tidak boleh kosong");
                           } else if (susername.length() < 5){
                               usernameText.setError("Username tidak boleng kosong dan minimal 5 karakter");
                           } else {
                               if (response.body().getStatus().equals("success")){
                                   if (semail.matches("([A-Za-z][A-Za-z0-9\\-\\.\\_]*)\\@([A-Za-z][A-Za-z0-9\\-\\_]*)(\\.[A-Za-z][A-Za-z0-9\\-\\_]*)+$")){
                                       Toast.makeText(getActivity(), "Data berhasil diubah", Toast.LENGTH_SHORT).show();
                                   } else {
                                       emailText.setError("Masukan email yang benar");
                                   }
                               }else if (response.body().getStatus().equals("failed")){
                                   Toast.makeText(getActivity(), "Data gagal diubah", Toast.LENGTH_SHORT).show();
                               }
                           }
                        }

                        @Override
                        public void onFailure(Call<PostPutDelUser> call, Throwable t) {

                        }
                    });
                }
            });
        }
        return view;
    }
}
